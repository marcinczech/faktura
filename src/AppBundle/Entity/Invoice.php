<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoiceRepository")
 */
class Invoice {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_number", type="string", length=255)
     */
    private $invoiceNumber;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float")
     */
    private $payment;

    /**
     * @var string
     *
     * @ORM\Column(name="goal", type="string", length=255)
     */
    private $goal;

    /**
     * @var int
     *
     * @ORM\Column(name="type_of_costs", type="integer")
     */
    private $typeOfCosts;

    /**
     * @var float
     *
     * @ORM\Column(name="member_payments", type="float", nullable=true)
     */
    private $memberPayments;

    /**
     * @var float
     *
     * @ORM\Column(name="one_percent", type="float", nullable=true)
     */
    private $onePercent;

    /**
     * @var float
     *
     * @ORM\Column(name="troops_resources", type="float", nullable=true)
     */
    private $troopsResources;

    /**
     * @var float
     *
     * @ORM\Column(name="collections", type="float", nullable=true)
     */
    private $collections;

    /**
     * @var float
     *
     * @ORM\Column(name="public_collections", type="float", nullable=true)
     */
    private $publicCollections;

    /**
     * @var float
     *
     * @ORM\Column(name="other_resources", type="float", nullable=true)
     */
    private $otherResources;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_payment", type="date")
     */
    private $datePayment;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var int
     * 
     * @ORM\Column(name="payment_type", type="integer", length=11, nullable=false)
     */
    private $paymentType;

    /**
     * Get paymentType
     * 
     * @return int
     */
    public function getPaymentType() {
        return $this->paymentType;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Invoice
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set invoiceNumber
     *
     * @param string $invoiceNumber
     *
     * @return Invoice
     */
    public function setInvoiceNumber($invoiceNumber) {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return string
     */
    public function getInvoiceNumber() {
        return $this->invoiceNumber;
    }

    /**
     * Set payment
     *
     * @param float $payment
     *
     * @return Invoice
     */
    public function setPayment($payment) {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Set paymentType
     * 
     * @param int $paymentType
     * 
     * @return Invoice
     */
    public function setPaymentType($paymentType) {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float
     */
    public function getPayment() {
        return $this->payment;
    }

    /**
     * Set goal
     *
     * @param string $goal
     *
     * @return Invoice
     */
    public function setGoal($goal) {
        $this->goal = $goal;

        return $this;
    }

    /**
     * Get goal
     *
     * @return string
     */
    public function getGoal() {
        return $this->goal;
    }

    /**
     * Set typeOfCosts
     *
     * @param integer $typeOfCosts
     *
     * @return Invoice
     */
    public function setTypeOfCosts($typeOfCosts) {
        $this->typeOfCosts = $typeOfCosts;

        return $this;
    }

    /**
     * Get typeOfCosts
     *
     * @return int
     */
    public function getTypeOfCosts() {
        return $this->typeOfCosts;
    }

    /**
     * Set memberPayments
     *
     * @param float $memberPayments
     *
     * @return Invoice
     */
    public function setMemberPayments($memberPayments) {
        $this->memberPayments = $memberPayments;

        return $this;
    }

    /**
     * Get memberPayments
     *
     * @return float
     */
    public function getMemberPayments() {
        return $this->memberPayments;
    }

    /**
     * Set onePercent
     *
     * @param float $onePercent
     *
     * @return Invoice
     */
    public function setOnePercent($onePercent) {
        $this->onePercent = $onePercent;

        return $this;
    }

    /**
     * Get onePercent
     *
     * @return float
     */
    public function getOnePercent() {
        return $this->onePercent;
    }

    /**
     * Set troopsResources
     *
     * @param float $troopsResources
     *
     * @return Invoice
     */
    public function setTroopsResources($troopsResources) {
        $this->troopsResources = $troopsResources;

        return $this;
    }

    /**
     * Get troopsResources
     *
     * @return float
     */
    public function getTroopsResources() {
        return $this->troopsResources;
    }

    /**
     * Set collections
     *
     * @param float $collections
     *
     * @return Invoice
     */
    public function setCollections($collections) {
        $this->collections = $collections;

        return $this;
    }

    /**
     * Get collections
     *
     * @return float
     */
    public function getCollections() {
        return $this->collections;
    }

    /**
     * Set publicCollections
     *
     * @param float $publicCollections
     *
     * @return Invoice
     */
    public function setPublicCollections($publicCollections) {
        $this->publicCollections = $publicCollections;

        return $this;
    }

    /**
     * Get publicCollections
     *
     * @return float
     */
    public function getPublicCollections() {
        return $this->publicCollections;
    }

    /**
     * Set otherResources
     *
     * @param float $otherResources
     *
     * @return Invoice
     */
    public function setOtherResources($otherResources) {
        $this->otherResources = $otherResources;

        return $this;
    }

    /**
     * Get otherResources
     *
     * @return float
     */
    public function getOtherResources() {
        return $this->otherResources;
    }

    /**
     * Set datePayment
     *
     * @param \DateTime $datePayment
     *
     * @return Invoice
     */
    public function setDatePayment($datePayment) {
        $this->datePayment = $datePayment;

        return $this;
    }

    /**
     * Get datePayment
     *
     * @return \DateTime
     */
    public function getDatePayment() {
        return $this->datePayment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Invoice
     */
    public function setComment($comment) {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Trip")
     * @ORM\JoinColumn(name="trip_id", referencedColumnName="id")
     */
    private $trip;

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * Set troopsID
     *
     * @param integer $troopsID
     *
     * @return Invoice
     */
    public function setTroopsID($troopsID) {
        $this->troopsID = $troopsID;

        return $this;
    }

    /**
     * Get troopsID
     *
     * @return integer
     */
    public function getTroopsID() {
        return $this->troopsID;
    }


    /**
     * Set trip
     *
     * @param \AppBundle\Entity\Trip $trip
     *
     * @return Invoice
     */
    public function setTrip(\AppBundle\Entity\Trip $trip = null)
    {
        $this->trip = $trip;

        return $this;
    }

    /**
     * Get trip
     *
     * @return \AppBundle\Entity\Trip
     */
    public function getTrip()
    {
        return $this->trip;
    }
}
