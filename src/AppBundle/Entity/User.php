<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Troops")
     * @ORM\JoinColumn(name="troops_id", referencedColumnName="id")
     */
    private $troops;

    /**
     * @ORM\OneToMany(targetEntity="Trip", mappedBy="trip")
     */
    private $trips;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="user")
     */
    private $users;

    public function __construct() {
        parent::__construct();
    }

    /**
     * Set region
     *
     * @param \AppBundle\Entity\Region $region
     *
     * @return User
     */
    public function setRegion(\AppBundle\Entity\Region $region = null) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \AppBundle\Entity\Region
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return User
     */
    public function addUser(\AppBundle\Entity\User $user) {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user) {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers() {
        return $this->users;
    }

    /**
     * Set troops
     *
     * @param \AppBundle\Entity\Troops $troops
     *
     * @return User
     */
    public function setTroops(\AppBundle\Entity\Troops $troops = null) {
        $this->troops = $troops;

        return $this;
    }

    /**
     * Get troops
     *
     * @return \AppBundle\Entity\Troops
     */
    public function getTroops() {
        return $this->troops;
    }


    /**
     * Add trip
     *
     * @param \AppBundle\Entity\Trip $trip
     *
     * @return User
     */
    public function addTrip(\AppBundle\Entity\Trip $trip)
    {
        $this->trips[] = $trip;

        return $this;
    }

    /**
     * Remove trip
     *
     * @param \AppBundle\Entity\Trip $trip
     */
    public function removeTrip(\AppBundle\Entity\Trip $trip)
    {
        $this->trips->removeElement($trip);
    }

    /**
     * Get trips
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrips()
    {
        return $this->trips;
    }
}
