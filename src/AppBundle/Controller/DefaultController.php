<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Utils\InvoiceGeneratorPDF;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Trip');
        $trips = $repository->findAll();
        $user = $this->get('security.token_storage')->getToken()->getUser();
//        $trips = $user->getTrips();
        $trips = $repository->findByUser($user->getId());
//dump($trips);die;
        return $this->render('default/index.html.twig', array(
                    'base_dir' => $this->get('kernel')->getRootDir() . '/..',
                    'trips' => $trips,
                    'user' => $user
        ));
    }

}
