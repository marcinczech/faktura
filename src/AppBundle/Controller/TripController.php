<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Trip;
use AppBundle\Form\TripType;

class TripController extends Controller {

    /**
     * @Route("/trip/create", name="create-trip")
     */
    public function createAction(Request $request) {
        $trip = new Trip();
        $trip->setName('Podaj nazwę');
        $form = $this->createForm(TripType::class, $trip);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $troops = $this->getDoctrine()->getRepository('AppBundle:Troops')->find($user->getTroops()->getId());
            $trip = $form->getData();
            $trip->setTroops($troops);
            $trip->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);
            $em->flush();
            return $this->redirectToRoute('trip', array('id' => $trip->getId()));
        }
        return $this->render('trip/create.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/trip/{id}", name="trip")
     */
    public function tripAction(Request $request) {
        $tripID = $request->get('id');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Trip');
        $trip = $repository->findOneById($tripID);
        return $this->render('trip/show.html.twig', array('base_dir' => $this->get('kernel')->getRootDir() . '/..', 'trip' => $trip));
    }
    /**
     * @Route("/trip/{id}/excel", name="trip2excel")
     */
    public function toExcelAction(Request $request) {
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $tripID = $request->get('id');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Trip');
        $trip = $repository->findOneById($tripID);
        $toExcel = new \AppBundle\Utils\Excel\toExcel($phpExcelObject, $trip);
$toExcel->getXls();        
    }

}
