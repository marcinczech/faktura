<?php

// src/AppBundle/Controller/RegistrationController.php

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;

class RegistrationController extends BaseController {

    public function registerAction(Request $request) {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            return $this->redirectToRoute('homepage');
        $user = new User();
        $form = $this->createForm(\AppBundle\Form\UserType::class, $user);
        $form->handleRequest($request);
        $process = $form->isSubmitted() && $form->isValid();
        if ($process) {
            try {

                $user = $form->getData();
                $user->setEnabled(true);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('fos_user_security_login', array('registration' => 'success'));
            } catch (\Exception $ex) {
                $errorMsg = 'Nie udało się dodać użytkownika, spróbuj wprowadzić inne dane.';
            }
        }
        return $this->render('Registration/register.html.twig', array(
                    'form' => $form->createView(),
                    'error' => $errorMsg,
        ));
    }

}
