<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Utils\InvoiceGeneratorPDF;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\InvoiceType;
use AppBundle\Entity\Invoice;
use TFox\MpdfPortBundle\Service\MpdfService;

class InvoiceController extends Controller {

    /**
     * @Route("/invoice/create/{tripID}", name="create-invoice")
     */
    public function createAction(Request $request) {
        $invoice = new Invoice();
        $form = $this->createForm(InvoiceType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $invoice = $form->getData();
            $trip = $this->getDoctrine()->getRepository('AppBundle:Trip')->find($request->get('tripID'));
            $invoice->setTrip($trip);
            $em = $this->getDoctrine()->getManager();
            $em->persist($invoice);
            $em->flush();
            return $this->redirectToRoute('trip', array('id' => $trip->getId()));
        }
        return $this->render('invoice/create.html.twig', array(
                    'form' => $form->createView(),
                    'tripID' => $request->get('tripID'),
        ));
    }

    /**
     * @Route("/invoice/edit/{invoiceId}", name="edit-invoice")
     */
    public function editAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Invoice');
        $invoice = $repository->findOneById($request->get('invoiceId'));
        $form = $this->createForm(InvoiceType::class, $invoice);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $invoice = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($invoice);
            $em->flush();
            return $this->redirectToRoute('trip', array('id' => $invoice->getTrip()->getId()));
        }
        $form->setData($invoice);
        return $this->render('invoice/edit.html.twig', array(
                    'form' => $form->createView(),
                    'tripID' => $invoice->getTrip()->getId(),
                    'invoiceId' => $invoice->getId()
        ));
    }

    /**
     * @Route("/invoice/delete/{invoiceId}", name="delete-invoice")
     */
    public function deleteAction(Request $request) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $repository = $this->getDoctrine()->getRepository('AppBundle:Invoice');
        $invoice = $repository->findOneById($request->get('invoiceId'));
        if ($invoice->getTrip()->getUser()->getId() == $user->getId()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->remove($invoice);
            $em->flush();
        }
        return $this->redirectToRoute('trip', array('id' => $invoice->getTrip()->getId()));
    }

    /**
     * @Route("/invoice/{id}", name="invoice2pdf")
     */
    public function invoiceAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Invoice');
        $invoice = $repository->findOneById($request->get('id'));
        $invoiceGenerator = new InvoiceGeneratorPDF($invoice);
        $pdfObject = $invoiceGenerator->prepare();
        $htmlFile = $this->renderView('pdfinvoice/opole.html.twig', [
            'header' => $pdfObject->getHeaderBlock(),
            'finance' => $pdfObject->getFinanceBlock(),
            'fund' => $pdfObject->getFundBlock(),
            'payment' => $pdfObject->getPaymentBlock(),
            'rootDir' => $this->get('kernel')->getRootDir() . '/..'
        ]);
        $mpdfService = new MpdfService();
        $mpdfClass = $mpdfService->getMpdf();
        $mpdfClass->WriteHTML($htmlFile);
        $content = $mpdfClass->Output();
        return new Response(
                $content
                , 200, array(
            'Content-Type' => 'application/pdf',
//        'Content-Disposition' => 'attachment; filename="file.pdf"'
        ));
    }

    /**
     * @Route("/invoices/{tripId}", name="invoices2pdf")
     */
    public function allInvoicesAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Trip');
        $trip = $repository->find($request->get('tripId'));
        $mpdfService = new MpdfService();
        $mpdfClass = $mpdfService->getMpdf();
        foreach ($trip->getInvoices() as $invoice) {
            $invoiceGenerator = new InvoiceGeneratorPDF($invoice);
            $pdfObject = $invoiceGenerator->prepare();
            $mpdfClass->addPage();
            $htmlFile = $this->renderView('pdfinvoice/opole.html.twig', [
                'header' => $pdfObject->getHeaderBlock(),
                'finance' => $pdfObject->getFinanceBlock(),
                'fund' => $pdfObject->getFundBlock(),
                'payment' => $pdfObject->getPaymentBlock(),
                'rootDir' => $this->get('kernel')->getRootDir() . '/..'
            ]);
            $mpdfClass->WriteHTML($htmlFile);
        }
        $content = $mpdfClass->Output();
        return new Response(
                $content
                , 200, array(
            'Content-Type' => 'application/pdf',
//        'Content-Disposition' => 'attachment; filename="file.pdf"'
        ));
    }

    /**
     * @Route("/get-invoice/{id}", name="invoice")
     */
    public function getInvoiceAction(Request $request) {
        $pdfGenerator = $this->get('spraed.pdf.generator');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Invoice');
        $invoice = $repository->findOneById($request->get('id'));

        $invoiceGenerator = new InvoiceGeneratorPDF($invoice);
        $pdfObject = $invoiceGenerator->prepare();
        $html = $this->renderView('pdfinvoice/opole.html.twig', [
            'header' => $pdfObject->getHeaderBlock(),
            'finance' => $pdfObject->getFinanceBlock(),
            'fund' => $pdfObject->getFundBlock(),
            'payment' => $pdfObject->getPaymentBlock(),
            'rootDir' => $this->get('kernel')->getRootDir() . '/..'
        ]);

        return new Response($pdfGenerator->generatePDF($html), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="out.pdf"'
                )
        );
    }

    /**
     * @Route("/invoices/{tripId}/to-excel", name="invoices2excel")
     */
    public function excelAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Trip');
        $trip = $repository->find($request->get('tripId'));
        $mpdfService = new MpdfService();
        $mpdfClass = $mpdfService->getMpdf();
        $invoices = $trip->getInvoices();




        return new Response(
                $content
                , 200, array(
            'Content-Disposition' => 'attachment; filename="file.pdf"'
        ));
    }

}
