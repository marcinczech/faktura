<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class InvoiceType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('invoiceNumber', TextType::class, array('label' => 'Numer faktury:'))
                ->add('type', ChoiceType::class, array(
                    'label' => 'Typ rozliczenia',
                    'choices' => array(
                        'Faktura' => 1,
                        'Rachunek' => 2,
                        'Delegacja' => 3,
                        'Rachunek kosztów podróży i inne' => 4
                    ),
                    'data' => 1,
                ))
                ->add('goal', TextType::class, array('label' => 'Cel zakupu'))
                ->add('payment', MoneyType::class, array('label' => 'Kwota', 'currency' => 'PLN'))
                ->add('typeOfCosts', ChoiceType::class, array(
                    'label' => 'Rodzaj poniesionych kosztów',
                    'choices' => array(
                        'Działalność odpłatna pożytku publicznego' => 1,
                        'Działalność nieodpłatna pożytku publicznego' => 2,
                        'Koszty administracyjne, w tym zużycie materiałów i energii, usługi obce, podatki i opłaty, wynagrodzenia, ubezpieczenia i inne świadczenia, amortyzacja' => 3,
                        'Koszty kampanii informacyjnej dot. 1% podatku' => 4,
                        'Pozostałe koszty' => 5,
                    ),
                    'data' => 2
                ))
                ->add('troopsResources', MoneyType::class, array('label' => 'Środki własne Chorągwi', 'currency' => 'PLN'))
                ->add('memberPayments', MoneyType::class, array('label' => 'Wpłaty uczestników', 'required' => false, 'currency' => 'PLN'))
                ->add('onePercent', MoneyType::class, array('label' => '1%', 'required' => false, 'currency' => 'PLN'))
                ->add('collections', MoneyType::class, array('label' => 'Składki', 'required' => false, 'currency' => 'PLN'))
                ->add('publicCollections', MoneyType::class, array('label' => 'Zbiórka publiczna', 'required' => false, 'currency' => 'PLN'))
                ->add('otherResources', MoneyType::class, array('label' => 'Inne (nawiązki, sponsorzy, inne)', 'required' => false, 'currency' => 'PLN'))
                ->add('datePayment', DateType::class, array('label' => 'Data', 'data' => new \DateTime('now')))
                ->add('paymentType', ChoiceType::class, array(
                    'label' => 'Sposób płatności',
                    'choices' => array(
                        'Gotówka' => 1,
                        'Przelew' => 2,
                        'Inne' => 3,
                    ),
                    'attr' => array(
                        'class' => 'info',
                        'data-info' => '<img src="/img/"/>'
                    )
                ))
                ->add('comment', TextType::class, array('label' => 'Uwagi', 'required'=>false))
                ->add('save', SubmitType::class, array('label' => 'Dodaj fakturę', 'attr'=>['class'=>'btn green']));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Invoice'
        ));
    }

}
