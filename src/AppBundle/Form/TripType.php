<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class TripType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', TextType::class, [
                    'label' => 'Nazwa wyjazdu',
                    'data' => ''
                ])
                ->add('type', ChoiceType::class, array(
                    'label' => 'Wybierz typ wyjazdu',
                    'choices' => array(
                        'Biwak, rajd, itp.' => 1,
                        'HAL/HAZ' => 2,
                    ),
                    'data' => 1
                ))
                ->add('save', SubmitType::class, array('label' => 'Dodaj wyjazd','attr'=>['class'=>'btn green']));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Trip'
        ));
    }

}
