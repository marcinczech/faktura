<?php

namespace AppBundle\Utils;

use AppBundle\Utils\Invoice\OpolskaInvoice;
use AppBundle\Utils\Invoice\Components\Invoice;
use AppBundle\Entity\Invoice as InvoiceModel;

class InvoiceGeneratorPDF {

    private $region = 'opolska';

    /**
     *
     * @var InvoiceModel
     */
    private $invoice;

    /**
     * 
     * @return Invoice
     */
    public function __construct(InvoiceModel $invoice) {
        $this->invoice = $invoice;
    }

    public function prepare() {
        $class = $this->getClass();
        $pdfObject = $class->prepare();
        return $pdfObject;
    }


    private function getClass() {
        switch ($this->region) {
            case 'opolska':
                return new OpolskaInvoice($this->invoice);
            default:
                throw new Exception('Nie obsługujemy takiej chorągwii ;)');
        }
    }

    public function getFileName() {
        switch ($this->region) {
            case 'opolska':
                return 'opole';
        }
        return true;
    }

}
