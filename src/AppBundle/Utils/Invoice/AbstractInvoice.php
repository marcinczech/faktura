<?php

namespace AppBundle\Utils\Invoice;

use AppBundle\Utils\Invoice\Components\Invoice as InvoiceBlock;

abstract class AbstractInvoice {

    /**
     *
     * @var InvoiceBlock
     */
    protected $invoiceBlock;

    public function __construct() {
        $this->invoiceBlock = new InvoiceBlock();
    }

}
