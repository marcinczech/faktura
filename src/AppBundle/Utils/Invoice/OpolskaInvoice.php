<?php

namespace AppBundle\Utils\Invoice;

use AppBundle\Utils\Invoice\InvoiceInterface;
use AppBundle\Utils\Invoice\AbstractInvoice;
use AppBundle\Entity\Invoice as InvoiceModel;
use AppBundle\Utils\Helper\NumberInWords;

class OpolskaInvoice extends AbstractInvoice implements InvoiceInterface {

    /**
     *
     * @var InvoiceModel
     */
    protected $invoice;

    public function __construct(InvoiceModel $invoice) {
        $this->invoice = $invoice;
        parent::__construct();
    }

    public function prepare() {
        $headerBlock = $this->invoiceBlock->getHeaderBlock();
        $headerBlock->setUnit($this->invoice->getTrip()->getTroops()->getName())
                ->setTaskName($this->invoice->getTrip()->getName())
                ->setInvoiceNumber($this->invoice->getInvoiceNumber())
                ->setInvoiceType($this->invoice->getType())
                ->setPrice($this->invoice->getPayment())
                ->setGoal($this->invoice->getGoal());

        $financeBlock = $this->invoiceBlock->getFinanceBlock();
        $financeBlock->setCollections($this->invoice->getCollections())
                ->setFinancingPrice($this->invoice->getPayment())
                ->setMemberPayments($this->invoice->getMemberPayments())
                ->setOnePercent($this->invoice->getOnePercent())
                ->setPublicCollection($this->invoice->getPublicCollections())
                ->setTroopsResources($this->invoice->getTroopsResources())
                ->setOthers($this->invoice->getOtherResources())
                ->setTypeOfCosts($this->invoice->getTypeOfCosts());
        if (false) {
            // TODO PROJECT FUND BLOCK
        }
        $paymentBlock = $this->invoiceBlock->getPaymentBlock();
        $paymentBlock->setPaymentComment($this->invoice->getComment())
                ->setPaymentType($this->invoice->getPaymentType())
                ->setPrice($this->invoice->getPayment())
                ->setTransactionDate($this->invoice->getDatePayment())
                ->setPriceInWords(NumberInWords::amountToWords($this->invoice->getPayment()));
        return $this->invoiceBlock;
    }

    private function kwotaslownie($kwota) {
        $kwota = explode(',', $kwota);

        $zl = preg_replace('/[^-\d]+/', '', $kwota[0]);
        $gr = preg_replace('/[^\d]+/', '', substr(isset($kwota[1]) ? $kwota[1] : 0, 0, 2));
        while (strlen($gr) < 2)
            $gr .= '0';

        echo slownie($zl) . ' ' . odmiana(Array('złoty', 'złote', 'złotych'), $zl) .
        (intval($gr) == 0 ? '' :
                ' ' . slownie($gr) . ' ' . odmiana(Array('grosz', 'grosze', 'groszy'), $gr));
    }

}
