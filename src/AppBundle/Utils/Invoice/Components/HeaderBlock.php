<?php

namespace AppBundle\Utils\Invoice\Components;

use AppBundle\Utils\Invoice\Components\AbstractBlock;

class HeaderBlock extends AbstractBlock {

    /**
     *
     * @var string
     */
    protected $unit;

    /**
     *
     * @var string
     */
    protected $taskName;

    /**
     * TODO pobieranie z bazy inoiceType
     * @var int
     */
    protected $invoiceType;

    /**
     *
     * @var string
     */
    protected $invoiceNumber;

    /**
     *
     * @var float
     */
    protected $price;

    /**
     *
     * @var string
     */
    protected $goal;

    public function getUnit() {
        return $this->unit;
    }

    public function getTaskName() {
        return $this->taskName;
    }

    public function getInvoiceType() {
        return $this->invoiceType;
    }

    public function getInvoiceNumber() {
        return $this->invoiceNumber;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getGoal() {
        return $this->goal;
    }

    public function setUnit($unit) {
        $this->unit = $unit;
        return $this;
    }

    public function setTaskName($taskName) {
        $this->taskName = $taskName;
        return $this;
    }

    public function setInvoiceType($invoiceType) {
        $this->invoiceType = $invoiceType;
        return $this;
    }

    public function setInvoiceNumber($invoiceNumber) {
        $this->invoiceNumber = $invoiceNumber;
        return $this;
    }

    public function setPrice($price) {
        $this->price = $price;
        return $this;
    }

    public function setGoal($goal) {
        $this->goal = $goal;
        return $this;
    }

}
