<?php

namespace AppBundle\Utils\Invoice\Components;

use AppBundle\Utils\Invoice\Components\AbstractBlock;

class FinanceBlock extends AbstractBlock {

    /**
     *
     * @var int
     */
    protected $typeOfCosts;

    /**
     *
     * @var float
     */
    protected $financingPrice;

    /**
     *
     * @var float
     */
    protected $troopsResources;

    /**
     *
     * @var float
     */
    protected $memberPayments;

    /**
     *
     * @var float
     */
    protected $onePercent;

    /**
     *
     * @var float
     */
    protected $collections;

    /**
     *
     * @var float
     */
    protected $publicCollection;

    /**
     *
     * @var float
     */
    protected $others;

    public function getFinancingPrice() {
        return $this->financingPrice;
    }

    public function getTroopsResources() {
        return $this->troopsResources;
    }

    public function getMemberPayments() {
        return $this->memberPayments;
    }

    public function getOnePercent() {
        return $this->onePercent;
    }

    public function getCollections() {
        return $this->collections;
    }

    public function getPublicCollection() {
        return $this->publicCollection;
    }

    public function getOthers() {
        return $this->others;
    }

    public function getTypeOfCosts() {
        return $this->typeOfCosts;
    }

    public function setTypeOfCosts($typeOfCosts) {
        $this->typeOfCosts = $typeOfCosts;
        return $this;
    }

    public function setFinancingPrice($financingPrice) {
        $this->financingPrice = $financingPrice;
        return $this;
    }

    public function setTroopsResources($troopsResources) {
        $this->troopsResources = $troopsResources;
        return $this;
    }

    public function setMemberPayments($memberPayments) {
        $this->memberPayments = $memberPayments;
        return $this;
    }

    public function setOnePercent($onePercent) {
        $this->onePercent = $onePercent;
        return $this;
    }

    public function setCollections($collections) {
        $this->collections = $collections;
        return $this;
    }

    public function setPublicCollection($publicCollection) {
        $this->publicCollection = $publicCollection;
        return $this;
    }

    public function setOthers($others) {
        $this->others = $others;
        return $this;
    }

}
