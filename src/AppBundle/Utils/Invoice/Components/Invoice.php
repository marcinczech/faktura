<?php

namespace AppBundle\Utils\Invoice\Components;

use AppBundle\Utils\Invoice\Components\HeaderBlock;
use AppBundle\Utils\Invoice\Components\FinanceBlock;
use AppBundle\Utils\Invoice\Components\FundBlock;
use AppBundle\Utils\Invoice\Components\PaymentBlock;

class Invoice {

    /**
     *
     * @var HeaderBlock
     */
    private $headerBlock;

    /**
     *
     * @var FinanceBlock
     */
    private $financeBlock;

    /**
     *
     * @var FundBlock
     */
    private $fundBlock;

    /**
     *
     * @var PaymentBlock
     */
    private $paymentBlock;

    /**
     * 
     * @return HeaderBlock
     */
    public function getHeaderBlock() {
        if (empty($this->headerBlock))
            $this->headerBlock = new HeaderBlock();
        return $this->headerBlock;
    }

    /**
     * 
     * @return FinanceBlock
     */
    public function getFinanceBlock() {
        if (empty($this->financeBlock))
            $this->financeBlock = new FinanceBlock();
        return $this->financeBlock;
    }

    /**
     * 
     * @return FundBlock
     */
    public function getFundBlock() {
        if (empty($this->fundBlock))
            $this->fundBlock = new FundBlock();
        return $this->fundBlock;
    }

    /**
     * 
     * @return PaymentBlock
     */
    public function getPaymentBlock() {
        if (empty($this->paymentBlock))
            $this->paymentBlock = new PaymentBlock();
        return $this->paymentBlock;
    }

}
