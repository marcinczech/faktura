<?php

namespace AppBundle\Utils\Invoice\Components;

use AppBundle\Utils\Invoice\Components\AbstractBlock;

class FundBlock extends AbstractBlock {

    /**
     * 13
     * @var float
     */
    protected $projectPrice;

    /**
     * 14
     * @var string
     */
    protected $costType;

    /**
     * 15
     * @var string
     */
    protected $estimateNumber;

    /**
     * 16 Kwota faktury w wysokości stanowi koszt kwalifikowany do zadania )wartość w zł) 
     * @var float
     */
    protected $eligibleInvoiceCost;

    /**
     * 17 zgodnie z umową zawartą z 
     * @var string
     */
    protected $donorName;

    /**
     * 18
     * @var string
     */
    protected $agreementNumber;

    /**
     * 18
     * @var string
     */
    protected $agreementDate;

    /**
     * 19
     * @var float
     */
    protected $invoiceDonatedPrice;

    /**
     * 20
     * @var float
     */
    protected $invoiceOwnPrice;

    public function getProjectPrice() {
        return $this->projectPrice;
    }

    public function getCostType() {
        return $this->costType;
    }

    public function getEstimateNumber() {
        return $this->estimateNumber;
    }

    public function getEligibleInvoiceCost() {
        return $this->eligibleInvoiceCost;
    }

    public function getDonorName() {
        return $this->donorName;
    }

    public function getAgreementNumber() {
        return $this->agreementNumber;
    }

    public function getAgreementDate() {
        return $this->agreementDate;
    }

    public function getInvoiceDonatedPrice() {
        return $this->invoiceDonatedPrice;
    }

    public function getInvoiceOwnPrice() {
        return $this->invoiceOwnPrice;
    }

    public function setProjectPrice($projectPrice) {
        $this->projectPrice = $projectPrice;
        return $this;
    }

    public function setCostType($costType) {
        $this->costType = $costType;
        return $this;
    }

    public function setEstimateNumber($estimateNumber) {
        $this->estimateNumber = $estimateNumber;
        return $this;
    }

    public function setEligibleInvoiceCost($eligibleInvoiceCost) {
        $this->eligibleInvoiceCost = $eligibleInvoiceCost;
        return $this;
    }

    public function setDonorName($donorName) {
        $this->donorName = $donorName;
        return $this;
    }

    public function setAgreementNumber($agreementNumber) {
        $this->agreementNumber = $agreementNumber;
        return $this;
    }

    public function setAgreementDate($agreementDate) {
        $this->agreementDate = $agreementDate;
        return $this;
    }

    public function setInvoiceDonatedPrice($invoiceDonatedPrice) {
        $this->invoiceDonatedPrice = $invoiceDonatedPrice;
        return $this;
    }

    public function setInvoiceOwnPrice($invoiceOwnPrice) {
        $this->invoiceOwnPrice = $invoiceOwnPrice;
        return $this;
    }

}
