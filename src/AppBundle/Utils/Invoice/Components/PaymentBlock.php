<?php

namespace AppBundle\Utils\Invoice\Components;

use AppBundle\Utils\Invoice\Components\AbstractBlock;

class PaymentBlock extends AbstractBlock {

    /**
     *
     * @var string
     */
    protected $paymentType;

    /**
     *
     * @var string
     */
    protected $paymentComment;

    /**
     *
     * @var float
     */
    protected $price;

    /**
     *
     * @var string
     */
    protected $priceInWords;

    /**
     *
     * @var string
     */
    protected $transactionDate;

    public function getPaymentType() {
        return $this->paymentType;
    }

    public function getPaymentComment() {
        return $this->paymentComment;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getPriceInWords() {
        return $this->priceInWords;
    }

    public function getTransactionDate() {
        return $this->transactionDate;
    }

    public function setPaymentType($paymentType) {
        $this->paymentType = $paymentType;
        return $this;
    }

    public function setPaymentComment($paymentComment) {
        $this->paymentComment = $paymentComment;
        return $this;
    }

    public function setPrice($price) {
        $this->price = $price;
        return $this;
    }

    public function setPriceInWords($priceInWords) {
        $this->priceInWords = $priceInWords;
        return $this;
    }

    public function setTransactionDate($transactionDate) {
        $this->transactionDate = $transactionDate;
        return $this;
    }

}
