<?php

namespace AppBundle\Utils\Invoice;

use AppBundle\Entity\Invoice as InvoiceModel;

interface InvoiceInterface {

    public function prepare();
}
